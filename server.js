var express =  require('express');
var cors = require('cors');
var path = require('path');
var ejsLayouts = require('express-ejs-layouts');

const app = express();
app.use(cors());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.set('layout', 'layouts/main');
app.use(ejsLayouts);

app.set('port', (process.env.PORT || 8080));

// INDEX PAGE
app.get('/', function(request, response) {
  response.send('Website working fine')
});



//GET ONE VEHICLE

app.get('/blog/vehicle/:vehicle_id', function(req, res, next) {
   
        var vehicle = {"id": 1,"manufacturer":"Benz","model":"4matic","fuel_type":"diesel", "color": "black", "features": "open roof"};
       
        res.render('pages/vehicle_detail', { title: 'Vehicle Details', vehicle: vehicle, layout: 'layouts/detail'} );
});




// GET ALL VEHICLES

app.get('/blog/vehicles', function(req, res) {
       
         var vehicles = [
             
      {"id": 1,"manufacturer":"Benz","model":"4matic","fuel_type":"diesel", "color": "black", "features": "open roof"},
      {"id": 2,"manufacturer":"Toyota","model":"camry","fuel_type":"fuel", "color": "red", "features": "very fast"},
      {"id": 3,"manufacturer":"Ford","model":"Mustang","fuel_type":"diesel", "color": "white", "features": "Very expensive"},
      {"id": 4,"manufacturer":"BMW","model":"model 1","fuel_type":"fuel", "color": "ox blood", "features": "very fine"}
          
          ];
        res.render('pages/vehicle_list', { title: 'Vehicle List', vehicles: vehicles, layout: 'layouts/detail'} );
});



// GET ONE AUTHOR
app.get('/blog/author/:author_id', function(req, res, next) {
        // Hard coding for simplicity. Pretend this hits a real database to get all authors in the system
        // dummy author response - no need to call database
        var author = {"id": 1,"first_name":"Bob","last_name":"Smith","email":"bob@gmail.com"};
        // change id = 2 and test for when :author_id
        res.render('pages/author_detail', { title: 'Author Details', author: author, layout: 'layouts/detail'} );
});

// GET ALL AUTHORS
app.get('/blog/authors', function(req, res) {
          // Hard coding for simplicity. Pretend this hits a real database to get all authors in the system
         // dummy authors response - no need to call database
         var authors = [
           {"id": 1,"firstName":"Bob","lastName":"Smith","email":"bob@gmail.com"},
          {"id": 2,"firstName":"Tammy","lastName":"Norton","email":"tnorton@yahoo.com"},
          {"id": 3,"firstName":"Tina","lastName":"Lee","email":"lee.tina@hotmail.com"}];
        res.render('pages/author_list', { title: 'Author List', authors: authors, layout: 'layouts/detail'} );
});
 

app.listen(app.get('port'), function() {
  console.log("Node app is running at localhost:" + app.get('port')); 
});